define([], function () {
    return {
        'exits': [
            {
                'node': 0,
                'scene': '001'
            }
        ],
        'graph': {
            'nodes': [
                {'id' : 0  , 'x'  : 50 , 'y' : 450 , 'depth' : 3} ,
                {'id' : 1  , 'x'  : 100 , 'y' : 450 , 'depth' : 3} ,

                {'id' : 2  , 'x'  : 225 , 'y' : 425 , 'depth' : 3} ,
                {'id' : 3  , 'x'  : 350 , 'y' : 400 , 'depth' : 3} ,
                {'id' : 4  , 'x'  : 500 , 'y' : 400 , 'depth' : 3} ,
                {'id' : 5  , 'x'  : 750 , 'y' : 440 , 'depth' : 3} ,

                {'id' : 7  , 'x'  : 200 , 'y' : 500 , 'depth' : 5  , 'scale': 1.1} ,
                {'id' : 8  , 'x'  : 250 , 'y' : 525 , 'depth' : 5  , 'scale': 1.1} ,
                {'id' : 9  , 'x'  : 350 , 'y' : 600 , 'depth' : 5  , 'scale': 1.4} ,
                {'id' : 10 , 'x'  : 600 , 'y' : 650 , 'depth' : 5  , 'scale': 1.4} ,
                {'id' : 14 , 'x'  : 700 , 'y' : 550 , 'depth' : 5  , 'scale': 1.2} ,

                {'id' : 11 , 'x'  : 20  , 'y' : 325 , 'depth': 0   , 'scale': 0.8} ,
                {'id' : 12 , 'x'  : 150 , 'y' : 300 , 'depth': 0   , 'scale': 0.8} ,
                {'id' : 13 , 'x'  : 250 , 'y' : 300 , 'depth': 0   , 'scale': 0.8} ,
            ],
            'edges': [
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [1, 7],
                [7, 8],
                [8, 9],
                [9, 10],
                [10, 14],
                [14, 5],
                [1, 11],
                [11, 12],
                [12, 13]
            ]
        },
        'furniture': {
            '01': {
                'x': 80,
                'y': 248,
                'depth': 2,
                'name': 'sink'
            },
            '02': {
                'x': 278,
                'y': 347,
                'depth': 4,
                'name': 'table'
            }
        },
        'objects': [
            {
                'id': 'pasta',
                'title': 'Pasta dish',
                'position': {
                    'x'  : 455 ,
                    'y' : 370 , 
                }
            }
        ]
    };
});
