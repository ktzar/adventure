define([], function () {
    return {
        'graph': {
            'nodes': [
                {'id' : 0  , 'x'  : 100 , 'y' : 450 , 'depth' : 3} ,
                {'id' : 1  , 'x'  : 100 , 'y' : 450 , 'depth' : 3} ,

                {'id' : 2  , 'x'  : 225 , 'y' : 425 , 'depth' : 3} ,
                {'id' : 3  , 'x'  : 350 , 'y' : 400 , 'depth' : 3} ,
                {'id' : 4  , 'x'  : 500 , 'y' : 400 , 'depth' : 3} ,
                {'id' : 5  , 'x'  : 750 , 'y' : 440 , 'depth' : 3} ,

                {'id' : 7  , 'x'  : 200 , 'y' : 500 , 'depth' : 5  , 'scale': 1.1} ,
                {'id' : 8  , 'x'  : 250 , 'y' : 525 , 'depth' : 5  , 'scale': 1.1} ,
                {'id' : 9  , 'x'  : 350 , 'y' : 600 , 'depth' : 5  , 'scale': 1.2} ,
                {'id' : 10 , 'x'  : 600 , 'y' : 650 , 'depth' : 5  , 'scale': 1.2} ,
                {'id' : 14 , 'x'  : 700 , 'y' : 550 , 'depth' : 5  , 'scale': 1.2} ,

                {'id' : 11 , 'x'  : 20  , 'y' : 325 , 'depth': 0   , 'scale': 0.8} ,
                {'id' : 12 , 'x'  : 150 , 'y' : 300 , 'depth': 0   , 'scale': 0.8} ,
                {'id' : 13 , 'x'  : 250 , 'y' : 300 , 'depth': 0   , 'scale': 0.8} ,
            ],
            'edges': [
                {'src': 0, 'dst': 1},
                {'src': 1, 'dst': 2},
                {'src': 2, 'dst': 3},
                {'src': 3, 'dst': 4},
                {'src': 4, 'dst': 5},
                {'src': 1, 'dst': 7},
                {'src': 7, 'dst': 8},
                {'src': 8, 'dst': 9},
                {'src': 9, 'dst': 10},
                {'src': 10,'dst': 14},
                {'src': 14,'dst': 5},

                {'src': 1,'dst': 11},
                {'src': 11,'dst': 12},
                {'src': 12,'dst': 13}
            ]
        },
        'furniture': {
            '01': {
                'x': 425,
                'y': 192,
                'depth': 2,
                'name': 'bus'
            },
            '02': {
                'x': 425,
                'y': 192,
                'depth': 4,
                'name': 'table'
            }
        },
        'objects': [
        ]
    };
});
