define(['../../pathfinding'], function (Pathfinding) {
    describe('Pathfinding', function () {
        var graph = {
            'nodes': [
                {'id': 0, 'x': 0, 'y': 200},
                {'id': 1, 'x': 50, 'y': 200},
                {'id': 6, 'x': 300, 'y': 200},
                {'id': 2, 'x': 100, 'y': 150},
                {'id': 3, 'x': 150, 'y': 150},
                {'id': 4, 'x': 200, 'y': 150},
                {'id': 5, 'x': 250, 'y': 150},
                {'id': 7, 'x': 100, 'y': 250},
                {'id': 8, 'x': 150, 'y': 250},
                {'id': 9, 'x': 200, 'y': 250},
                {'id': 10, 'x': 250, 'y': 250},
            ],
            'edges': [
                {'id': 0, 'src': 0, 'dst': 1},
                {'id': 1, 'src': 1, 'dst': 2},
                {'id': 2, 'src': 2, 'dst': 3},
                {'id': 3, 'src': 3, 'dst': 4},
                {'id': 4, 'src': 4, 'dst': 5},
                {'id': 5, 'src': 5, 'dst': 6},
                {'id': 6, 'src': 1, 'dst': 7},
                {'id': 7, 'src': 7, 'dst': 8},
                {'id': 8, 'src': 8, 'dst': 9},
                {'id': 9, 'src': 9, 'dst': 10},
                {'id': 10,'src': 10,'dst': 6}
            ]
        };

        var pathfinding = new Pathfinding(graph);

        it('calculates distance from a point to a segment', function () {
            var seg1 = {x: 10, y: 0},
                seg2 = {x: 10, y: 10},
                p1, distance;

            p1 = {x: 0, y: 0},
            distance = pathfinding.distToSegment(p1, seg1, seg2);
            expect(distance).toBe(10);

            p1 = {x: 0, y: 20};
            distance = pathfinding.distToSegment(p1, seg1, seg2);
            expect(distance).toBe(Math.sqrt(200));

            p1 = {x: 0, y: -20};
            distance = pathfinding.distToSegment(p1, seg1, seg2);
            expect(distance).toBe(Math.sqrt(500));
        });

        it('fails constructing if points not in path', function () {
            var route = pathfinding.routeFromTo({x: 10, y: 10}, {x: 40, y: 40})
            expect(route).toBeFalsy();
        });

        it('constructs if points are in path', function () {
            var route = pathfinding.routeFromTo({x: 10, y: 200}, {x: 100, y: 250})
            expect(route).toBeFalsy();
        });

        /*
        it('returns a route', function () {
            var route = pathfinding.routeFromTo({x: 50, y: 200}, {x: 100, y: 250})
            expect(route.length).toBeGreaterThan(0);
        });
        */

    });
});
