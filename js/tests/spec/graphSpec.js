define(['../../graph'], function (Graph) {
    describe('Graph', function () {
        var graphData = {
            'nodes': [
                {'id': 0, 'x': 0, 'y': 200},
                {'id': 1, 'x': 50, 'y': 200},
                {'id': 2, 'x': 100, 'y': 150},
                {'id': 3, 'x': 150, 'y': 150},
                {'id': 4, 'x': 200, 'y': 150},
                {'id': 5, 'x': 250, 'y': 150},
                {'id': 6, 'x': 300, 'y': 200},
                {'id': 7, 'x': 100, 'y': 250},
                {'id': 8, 'x': 150, 'y': 250},
                {'id': 9, 'x': 200, 'y': 250},
                {'id': 10, 'x': 250, 'y': 250},
            ],
            'edges': [
                {'id': 'a1', 'src': 0, 'dst': 1},
                {'id': 'a2', 'src': 1, 'dst': 2},
                {'id': 'a3', 'src': 2, 'dst': 3},
                {'id': 'a4', 'src': 3, 'dst': 4},
                {'id': 'a5', 'src': 4, 'dst': 5},
                {'id': 'a6', 'src': 5, 'dst': 6},
                {'id': 'b1', 'src': 1, 'dst': 7},
                {'id': 'b2', 'src': 7, 'dst': 8},
                {'id': 'b3', 'src': 8, 'dst': 9},
                {'id': 'b4', 'src': 9, 'dst': 10},
                {'id': 'b5','src': 10,'dst': 6}
            ]
        };

        var graph = new Graph(graphData);

        it('returns the number of nodes', function () {
            expect(graph.numberNodes()).toBe(11);
        });

        it('returns the neighbours of a node', function () {
            var neighbours = graph.getNeighbours(6);
            expect(neighbours).toContain(5);
            expect(neighbours).toContain(10);
        });

        it ('returns the closest node to a coordinate', function () {
            expect(graph.closestNode(51, 199)).toBe(1);
            expect(graph.closestNode(49, 201)).toBe(1);
            expect(graph.closestNode(150, 201)).toBe(8);
        });

        it('finds a node', function () {
            expect(graph.findNode(-11, -11)).toBe(false);
            expect(graph.findNode(0, 200)).toBe(0);
        });

        it('finds a trivial route', function () {
            var path = graph.getShortestPath(8, 8);
            expect(path).toEqual([8, 8]);
        });

        it('finds the shortest route', function () {
            var path = graph.getShortestPath(1, 2);
            expect(path).toEqual([1, 2]);
        });

        it('finds another short route', function () {
            var path = graph.getShortestPath(6, 4);
            expect(path).toEqual([6, 5, 4]);
        });

        it('finds a normal route', function () {
            path = graph.getShortestPath(5, 9);
            expect(path).toEqual([5,6,10,9]);
        });

        it('finds another normal route', function () {
            path = graph.getShortestPath(0, 5);
            expect(path).toEqual([0,1,2,3,4,5]);
        });
    });
});
