define(['./scene', './player'], function (Scene, Player) {
    var game = new Phaser.Game(800, 600, Phaser.AUTO, 'adventure', { preload: preload, create: create, update: update });

    game.sceneExit = function (newScene) {
        game.state.start('scene', true, true, newScene);
    };

    var sceneId = '002';

    function preload() {
        game.state.add('scene', Scene);
    }

    function create() {
        game.state.start('scene', true, true, sceneId);
    }

    function update() {
    }
});
