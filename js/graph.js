define([], function () {
    return function (graphData, debugBoolean) {

        function debug(msgs) {
            if (debugBoolean) {
                console.log(msgs);
            }
        }

        this.numberNodes = function () {
            return graphData.nodes.length;
        };

        this.closestNode = function (x, y) {
            var closest = false, closestDistance = Infinity, distance;
            function sqr(a) { return a * a; }
            for (var n in graphData.nodes) {
                node = graphData.nodes[n];
                distance = sqr(node.x - x) + sqr(node.y - y);
                if (distance < closestDistance) {
                    closest = node.id;
                    closestDistance = distance;
                }
            }
            return closest;
        };

        this.getNeighbours = function(node) {
            var neighbours = [], s;
            for (s in graphData.edges) {
                edge  = graphData.edges[s];
                if (edge[0] === node) {
                    neighbours.push(edge[1]);
                }
                if (edge[1] === node) {
                    neighbours.push(edge[0]);
                }
            }
            return neighbours;
        };

        this.getNode = function (id) {
            var n;
            for (n in graphData.nodes) {
                if (graphData.nodes[n].id === id) {
                    return graphData.nodes[n];
                }
            }
            return false;
        };

        this.findNode = function(x, y) {
            var n, node;
            for (n in graphData.nodes) {
                node = graphData.nodes[n];
                if (node.x === x && node.y === y) {
                    return node.id;
                }
            }
            return false;
        };

        this.getShortestPath = function (start, end) {
            var path, that = this;

            function getRoute(start, end, visited) {
                debug('getRoute(',start,',', end,',', visited,')');
                var neighbours = that.getNeighbours(start),
                    n;
                if (start === end) {
                    return [start, end];
                }
                //Check if a neighbour is the end
                debug('Neighbours of ', start, ': ', neighbours);
                if (neighbours.indexOf(end) !== -1) {
                    debug('Target found!!!!: ', visited);
                    return visited.concat([start, end]);
                }
                //Node is not a neighbour, add ourselves to visited, and ask non-visited neighbours for paths
                var chosenPath = false, tmpPath;
                for(n in neighbours) {
                    if (visited.indexOf(neighbours[n]) === -1) {
                        tmpPath = getRoute(neighbours[n], end, visited.concat(start));
                        if (tmpPath === false) {
                            continue;
                        }
                        if (chosenPath === false || tmpPath.length < chosenPath.length) {
                            chosenPath = tmpPath;
                        }
                    }
                }
                if (chosenPath === false) {
                    debug(visited, ' is a dead end');
                    return false;
                } 
                return chosenPath;
            }
            route = getRoute(start, end, []);
            if (route === false) {
                debug('No route from ', start, ' to ', end);
                return false;
            } 
            return route;
        };
    };
});
