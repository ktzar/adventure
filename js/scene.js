define(['./player', './text', './graph', 'underscore'], function (Player, Text, Graph, _) {
    debug = true;
    var engaged = false;

    return function(game) {
        var sceneData, objects, furniture, graph, actor, bg,
        foreFurniture, backFurniture, code,
        cache = {
            currentLabel: false
        },
        methods = {
            showText: function (sprite, event) {
                if (typeof sprite.label === 'undefined') {
                    var text = Text.createSpriteLabel(game, sprite);
                    sprite.label = text;
                }
            },
            isExit: function (id) {
                var i;
                for (i in sceneData.exits) {
                    if (sceneData.exits[i].node === id) {
                        return sceneData.exits[i].scene;
                    }
                }
                return false;
            },
            hideText: function (sprite, event) {
                sprite.label.destroy();
                delete sprite.label;
            },
            loadFurniture: function (i) {
                var obj = furniture[i];
                backFurniture.create(
                    obj.x,
                    obj.y,
                    'furniture_' + obj.name
                );
                sprite = foreFurniture.create(
                    obj.x,
                    obj.y,
                    'furniture_' + obj.name
                );
                sprite.depth = obj.depth;
            },
            loadObject: function (i) {
                var obj = objects[i],
                sprite = game.add.sprite(
                    obj.position.x,
                    obj.position.y,
                    'object_' + obj.id
                );
                sprite.originalObject = obj;
                sprite.inputEnabled = true;
                sprite.input.useHandCursor = true;
                sprite.events.onInputOver.add(methods.showText, this);
                sprite.events.onInputOut.add(methods.hideText, this);
            },
            distributeFurniture: function (actorZ) {
                var i, sprite;
                for (i = 0; i < foreFurniture.length ; i ++) {
                    sprite = foreFurniture.getAt(i);
                    sprite.visible = sprite.depth > actorZ;
                }
            }
        };

        this.init = function (sceneCode) {
            code = sceneCode;
            console.log('Load Scene: '+code);
        };

        this.preload = function (preloadedCallback) {
            console.log('preload');

            var scenePath = '../assets/scenes/' + code;
            actor = new Player(game);
            actor.preload();

            game.load.image('scene_bg', scenePath + '/bg.jpg');

            require([scenePath + '/data'], function (data) {
                var i, obj;
                sceneData = data;
                objects = sceneData.objects;
                for (i in objects) {
                    obj = objects[i];
                    game.load.image(
                        'object_'+objects[i].id,
                        scenePath + '/' + objects[i].id + '.png'
                    );
                }
                furniture = sceneData.furniture;
                for (i in furniture) {
                    game.load.image(
                        'furniture_' + furniture[i].name ,
                        scenePath + '/furniture_' + i + '.png'
                    );
                }

                graph = new Graph(sceneData.graph);
                if (typeof preloadedCallback === 'function') {
                    preloadedCallback();
                }

            });
        };

        this.destroy = function () {
            if (backFurniture) backFurniture.destroy();
            if (foreFurniture) foreFurniture.destroy();
            if (actor) actor.destroy();
        },

        this.create = function () {
            console.log('create');
            var i, n;
            bg = game.add.sprite(0, 0, 'scene_bg');
            backFurniture = game.add.group();
            actor.create();
            foreFurniture = game.add.group();

            
            for (i in objects) {
                methods.loadObject(i);
            }

            for (i in furniture) {
                methods.loadFurniture(i);
            }

            if (debug) {
                for (n in sceneData.graph.nodes) {
                    var node = sceneData.graph.nodes[n];
                    game.debug.geom(
                            new Phaser.Circle(node.x, node.y, 10)
                            );
                    style = { font: "25px Arial", fill: "#ff0044", align: "center" };
                    game.add.text(node.x, node.y, node.id.toString(), style);
                }
                for (n in sceneData.graph.edges) {
                    var edge = sceneData.graph.edges[n];
                    game.debug.geom(
                        new Phaser.Line(
                            graph.getNode(edge[0]).x,
                            graph.getNode(edge[0]).y,
                            graph.getNode(edge[1]).x,
                            graph.getNode(edge[1]).y
                        )
                    );
                }
            }
        };

        this.moveActor = function (dst) {
            console.log('Actor position', actor.getPosition());
            var startNode = graph.closestNode(actor.getPosition().x, actor.getPosition().y + actor.getDimensions().height);
            var dstNode = graph.closestNode(dst.x, dst.y);

            var path = graph.getShortestPath(startNode, dstNode);
            console.log('path from ' + startNode + ' to ' + dstNode, path);

            var i = 0;
            function moveToNextPoint() {
                var nodeCoords, newScene;
                i++;
                if (i < path.length) {
                    nodeCoords = graph.getNode(path[i]);
                    newScene = methods.isExit(nodeCoords.id);
                    if (newScene) {
                        console.log('Arrived to an exit, going to ', newScene);
                        game.sceneExit(newScene);
                    }
                    if (!nodeCoords.scale) {
                        nodeCoords.scale = 1;
                    }
                    actor.goTo(
                        {x: nodeCoords.x, y: nodeCoords.y},
                        nodeCoords.scale,
                        moveToNextPoint
                    );
                    methods.distributeFurniture(nodeCoords.depth);
                }
            }
            moveToNextPoint();
        };

        this.update = function () {
            if (engaged === false && game.input.activePointer.isDown) {
                engaged = true;
                this.moveActor({
                    x: game.input.x,
                    y: game.input.y
                });
            }
            if (game.input.activePointer.isUp) {
                engaged = false;
            }
        };
    };
});
