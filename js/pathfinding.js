define(['./graph'], function (Graph) {
    return function (graphData) {
        graph = new Graph(graphData);

        function sqr(x) {
            return x * x;
        }

        function dist2(seg1, seg2) {
            return sqr(seg1.x - seg2.x) + sqr(seg1.y - seg2.y);
        }

        function distToSegmentSquared(point, seg1, seg2) {
          var l2 = dist2(seg1, seg2);
          //Segment is a single point
          if (l2 === 0) {
              return dist2(point, seg1);
          }
          //Find out if the point is in between of the space define in between the segment's edges' normals
          var t = (
              (point.x - seg1.x) * (seg2.x - seg1.x) +
              (point.y - seg1.y) * (seg2.y - seg1.y)
          ) / l2;
          //Beyond seg2
          if (t < 0) {
              return dist2(point, seg1);
          }
          //Beyond seg1
          if (t > 1) {
              return dist2(point, seg2);
          }
          //In between, distance is the normal proyection to the point
          return dist2(
              point, 
              {
                  x: seg1.x + t * (seg2.x - seg1.x),
                  y: seg1.y + t * (seg2.y - seg1.y)
              }
          );
        }

        this.distToSegment = function (point, seg1, seg2) {
            return Math.sqrt(distToSegmentSquared(point, seg1, seg2));
        }

        this.closestSegment = function (point, segments) {
            var closest = false, closestDistance = Infinity, segment;
            for (var i in segments) {
                if (distToSegmentSquared(point, segments[i]) < closestDistance) {
                    closest = i;
                }
            }
            return closest;
        }

        this.routeFromTo = function (p1, p2) {
            var s, segment, p1_found = false, p2_found = false,
                start, end;

            start = graph.findNode(p1.x, p1.y);
            end = graph.findNode(p2.x, p2.y);
            //Check that the points exist
            if (start === false || end === false) {
                return false;
            } else {
                return graph.getShortestPath(start, end);
            }
        }
    };
});
