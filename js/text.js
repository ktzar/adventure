define([], function () {
    var FONT_SIZE = 20;
    return {
        createSpriteLabel: function (game, sprite) {
            var text = game.add.text(
                sprite.x + sprite.width / 2,
                sprite.y - FONT_SIZE,
                sprite.originalObject.title
            );
            text.anchor.set(0.5);
            text.align = 'center';

            //	Font style
            text.font = 'Verdana';
            text.fontSize = FONT_SIZE;
            text.fontWeight = 'normal';
            text.fill = '#ffffff';
            text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 1);
            text.stroke = '#000000';
            text.strokeThickness = 6;
            
            return text;
        }
    };
});
