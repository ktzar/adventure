define([], function () {
    return function(game) {
        var hero, tween;

        function distance(point1, point2) {
            return game.math.distance(point1.x, point1.y, point2.x, point2.y);
        }

        this.getPosition = function () {
            return hero.position;
        };
        
        this.getDimensions = function () {
            return hero.getBounds();
        };

        this.preload = function () {
            game.load.spritesheet('hero', '../assets/sprites/elaine.png', 25, 50, 12);
        };

        this.destroy = function () {
            hero.destroy();
        };

        this.create = function () {
            hero = game.add.sprite(200, 200, 'hero');
            hero.anchor.setTo(0.5, 0);
            hero.scale.y = hero.scale.x = 2.5;
            hero.animations.add('front', [0,1,2,3]);
            hero.animations.add('side', [4,5,6,7]);
            hero.animations.add('back', [8,9,10,11]);
        };

        this.goTo = function (dstPoint, scale, finished) {
            var dist, angle;
            dstPoint.y -= hero.height;
            angle = Phaser.Math.angleBetween(
                    dstPoint.x, dstPoint.y,
                    hero.x, hero.y
            ) + Math.PI;

            var animationName = 'side';
            hero.scale.x = hero.scale.y = 2.5;
            if (angle > Math.PI * (7/4) ||  angle < Math.PI/4) {
                hero.scale.x = Math.abs(hero.scale.x);
            } else if (angle > 3*Math.PI/4 && angle < 5/4 * Math.PI) {
                hero.scale.x = -Math.abs(hero.scale.x);
            } else if (angle > Math.PI/4 && angle < 3/4 * Math.PI) {
                animationName = 'front';
            } else if (angle > 5*Math.PI/4 && angle < 7/4 * Math.PI) {
                animationName = 'back';
            }
            if (scale) {
                hero.scale.x *= scale;
                hero.scale.y *= scale;
            }

            tween && tween.pause();
            tween = game.add.tween(hero).to(
                dstPoint,
                distance(hero.position, dstPoint) * 6,
                Phaser.Easing.Linear.None,
                true, 0, false
            );

            hero.animations.play(animationName, 10, true);
            tween.onComplete.add(function () {
                hero.animations.stop();
                finished();
            });
        }
    }
});
